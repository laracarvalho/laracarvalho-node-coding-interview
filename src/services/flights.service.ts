import { ObjectId } from 'mongoose'
import { FlightsModel } from '../models/flights.model'
import { PersonsService } from '../services/persons.service'

export class FlightsService {
    getAll() {
        return FlightsModel.find()
    }

    async addToFlight(personId: string | ObjectId, flightId: string | ObjectId) {
        return FlightsModel.updateOne({ id: flightId }, {
            $addToSet: personId
        })
    }
}
