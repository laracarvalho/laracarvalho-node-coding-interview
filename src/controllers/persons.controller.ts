import { JsonController, Get, Post, Body, Delete } from 'routing-controllers'
import { PersonsService } from '../services/persons.service'

const personsService = new PersonsService()

@JsonController('/persons', { transformResponse: false })
export class PersonsController {
    @Get()
    async getAll() {
        const data = await personsService.getAll()
            .catch(error => {
                return {
                    status: 404,
                    error: 'No data available'
                }
            })

        return {
            status: 200,
            data,
        }
    }

    @Post('', { transformResponse: false })
    async create(@Body() p: any) {
        await personsService.create(p)
        return {
            status: 201,
        }
    }

    @Delete('/:id', { transformResponse: false })
    async delete(@Body() p: any) {
        await personsService.delete(p)
        
        return {
            status: 200,
        }
    }
}
