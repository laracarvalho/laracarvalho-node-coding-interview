import { JsonController, Get, Post } from 'routing-controllers'
import { FlightsService } from '../services/flights.service'

const flightsService = new FlightsService()

@JsonController('/flights')
export default class FlightsController {
    @Get('', { transformResponse: false })
    getAll() {
        return {
            status: 200,
            data: flightsService.getAll(),
        }
    }

    @Post('/:id', { transformResponse: false })
    async addToFlight(personId: string, flightId: string) {

        const person = await flightsService.addToFlight(personId, flightId)

        return {
            status: 200,
            data: person,
        }
    }
}
