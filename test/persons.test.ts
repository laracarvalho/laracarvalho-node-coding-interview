const assert = require('assert')
import { PersonsModel } from '../src/models/persons.model'
import { db } from '../src/memory-database'
import { after } from 'mocha'
import { PersonsController } from '../src/controllers/persons.controller'

beforeEach(async () => {
    //await db({ test: true })
})

describe('Persons Model', async () => {

    it('Returns correct error when no person registered', async () => {
        // Arrange
        const controller = new PersonsController()

        // Act

        const response = await controller.getAll()
        
        // Assert
        assert.equal(response.status, 404)
    })
    it('Allows to create two persons with different emails', async () => {
        // Arrange
        // Act
        // Assert
    })

    it('Prevents creating a person that already exists on the Database', async () => {
        // Arrange
        // Act
        // Assert
    })
})
